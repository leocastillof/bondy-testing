### Run bondy docker
Run the bondy docker using the latest available image:

NOTE: in the `bondy/etc` directory the are 2 files to be able to config bondy:
- `bondy.conf`
- `security.json`: here you have the realms (auth methods, users, groups and sources) configuration

Run the command below from the root directory:

```bash
docker run \
--rm \
-e BONDY_ERL_NODENAME=bondy1@127.0.0.1 \
-e BONDY_ERL_DISTRIBUTED_COOKIE=bondy \
-u 0:1000 \
-p 18080:18080 \
-p 18081:18081 \
-p 18082:18082 \
-p 18083:18083 \
-p 18084:18084 \
-p 18085:18085 \
-u 0:1000 \
-v "./bondy/etc:/bondy/etc" \
-v "./bondy/data:/bondy/data" \
--name bondy \
leapsight/bondy:1.0.0-rc.6
```

### Retrieve the configured realms using the Bondy management api
Run the command below from any terminal:

```bash
curl http://localhost:18081/realms | jq
```

Example run (in Realm)
---

Open a terminal and navigate to the directory where the "ws_anon" script is located.

```bash
npm install
```

This will install the necessary dependencies.

### 1) Create Realm

```bash
node ws_realm_create.js ws://localhost:18080/ws com.leocastillo
```

### 2) List realm
```bash
node ws_anon.js ws://localhost:18080/ws com.leocastillo
```

### 3) Call all realms
```bash
node ws_realm_caller.js ws://localhost:18080/ws
```

### 4) Publish a topic
```bash
node ws_realm_publish.js ws://localhost:18080/ws
```


### Added 23-01-2024
---

Added the following files:
- `ws_group_add.js`
- `ws_group_list.js`
- `ws_users_add.js`

NOTE: Its execution is similar to the logic of `ws_anon.js` (default file).
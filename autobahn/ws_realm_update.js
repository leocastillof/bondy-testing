#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 7) {
    console.error('Usage: node ws_realm_update.js <uri> <description> <is_prototype> <is_sso_realm> <allow_connections> <authmethods>');
    process.exit(1);
}

// Extract arguments
var [uri, description, is_prototype, is_sso_realm, allow_connections, ...authmethods] = args;

// Anonymous connection
var connection = new autobahn.Connection({
   url: "ws://localhost:18080/ws",
   realm: "com.leapsight.bondy",
   authmethods : ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function update_realm(session, realmUri, realmData) {
    session.call('bondy.realm.update', [realmUri, realmData]).then(
        function (updatedRealm) {
            log_separator();
            console.log('Realm updated successfully. Updated Realm: ', updatedRealm);
        },
        function (err) {
            log_separator();
            console.log('Error updating realm', err);
        }
    );
}

connection.onopen = function(session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // CREATE realm
    var updatedRealmData = {
        uri: uri,
        description: description,
        is_prototype: Boolean(is_prototype),
        is_sso_realm: Boolean(is_sso_realm),
        allow_connections: Boolean(allow_connections),
        authmethods: authmethods
        // Add other properties as needed
    };

    update_realm(session, uri, updatedRealmData);
};

connection.onclose = function(reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

var connection = new autobahn.Connection({
   url: "ws://localhost:18080/ws",
   realm: "com.leapsight.bondy",
   authmethods : ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

connection.onopen = function(session, details) {
    console.log("Connected: ", JSON.stringify(details));

    session.register('bondy.realm.create', function (args) {
        return createRealm(session, args[0]);
    });

    log_separator();
    console.log('Backend ready to handle realm creation requests.');
    log_separator();
};

connection.onclose = function(reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
    log_separator();
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

function createRealm(session, newRealmConfig) {
    log_separator();
    console.log('Received request to create a realm:', newRealmConfig);

    // Aquí debes implementar la lógica para crear el reino con los datos proporcionados.
    // Puedes usar la biblioteca y lógica existente que ya tienes en este script.
    
    // Simplemente lograremos el éxito por ahora.
    return 'Success! Realm created.';
}

#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');
const readline = require('readline');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 2) {
    console.error('Usage: node ws_group_delete.js <realmUri> <groupName>');
    process.exit(1);
}

// Extract arguments
var [realmUri, groupName] = args;

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function delete_group(session, realmUri, groupName) {
    session.call(
        'bondy.group.delete',
        [realmUri, groupName]
    ).then(
        function (res) {
            log_separator();
            console.log('Group Deleted: ', res);
            // Optionally, you can keep the connection open for further interactions
            // connection.close();
        },
        function (err) {
            log_separator();
            console.log('Failed to delete group', err.error, err.kwargs);
            // Optionally, you can keep the connection open for further interactions
            // connection.close();
        }
    );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // Call the function to delete the group
    delete_group(session, realmUri, groupName);
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
    // Optionally, you can add logic to handle reconnection here if needed
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

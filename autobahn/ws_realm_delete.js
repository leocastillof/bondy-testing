#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 2) {
    console.error('Usage: node ws_anon_delete.js <realmUri> <force>');
    process.exit(1);
}

// Extract arguments
var [realmUri, force] = args;

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // DELETE Realm
    session.call('bondy.realm.delete', [realmUri], { force: Boolean(force) }).then(
        function (res) {
            log_separator();
            console.log('Realm successfully removed. Result: ', res);
        },
        function (err) {
            log_separator();
            console.log('Error deleting Realm', err);
        }
    );
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

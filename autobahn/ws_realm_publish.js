#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Obtén los argumentos de la línea de comandos
var args = process.argv.slice(2);

// Verifica si se proporcionaron suficientes argumentos
if (args.length < 2) {
    console.error('Usage: node ws_anon_publish.js <topic> <message>');
    process.exit(1);
}

// Extrae los argumentos
var [topic, message] = args;

// Conexión anónima
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function do_publish(session) {
    session
        .publish(
            topic,
            [message],
            { id: session.id },
            {
                exclude_me: false,
                acknowledge: true,
                retain: false,
                _retained_ttl: 50
            }
        )
        .then(
            function (publication) {
                log_separator();
                console.log(`Published to '${topic}': ${message}`);
            },
            function (err) {
                log_separator();
                console.log("Error to publish ", err);
            }
        );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));
    do_publish(session);
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 1) {
    console.error('Usage: node ws_group_list.js <realmUri>');
    process.exit(1);
}

// Extract arguments
var [realmUri] = args;

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function list_groups(session, realmUri) {
    session.call(
        'bondy.group.list',
        [realmUri]
    ).then(
        function (groups) {
            log_separator();
            console.log('List of Groups: ', groups);
        },
        function (err) {
            log_separator();
            console.log('Failed to list groups', err);
        }
    );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    list_groups(session, realmUri);
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}
#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 3) {
    console.error('Usage: node ws_group_add_group.js <realmUri> <groupName> <updatedGroupData>');
    process.exit(1);
}

// Extract arguments
var [realmUri, groupName, updatedGroupData] = args;

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function update_group(session, realmUri, groupName, updatedGroupData) {
    session.call(
        'bondy.group.update',
        [realmUri, groupName, updatedGroupData]
    ).then(
        function (res) {
            log_separator();
            console.log('Group Updated: ', res);
        },
        function (err) {
            log_separator();
            console.log('Failed to update group', err);
        }
    );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // Convert updatedGroupData to an object
    try {
        updatedGroupData = JSON.parse(updatedGroupData);
    } catch (e) {
        log_separator();
        console.log('Error parsing updatedGroupData. Please provide valid JSON.');
        connection.close();
        return;
    }

    update_group(session, realmUri, groupName, updatedGroupData);
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

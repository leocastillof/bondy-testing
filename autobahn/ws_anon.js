#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');
const WebSocket = require('ws');

// Anonymous connection
var connection = new autobahn.Connection({
   url: "ws://localhost:18080/ws",
   realm: "com.leapsight.demo",
   authmethods : ["anonymous"]
});


function log_separator() {
    console.log("--------------------------------------------------------");
}

connection.onopen = function(session, details) {
    console.log("Connected: ", JSON.stringify(details));

     // SUBSCRIBE to a topic and receive event

   function on_event (args) {
        log_separator();
        console.log("received event", args);
   }

    session.subscribe(
        'com.leocastillo',
        on_event,
        {match:'exact'}
    ).then(
        function (sub) {
            log_separator();
            console.log('subscribed ', sub);
        },
        function (err) {
            log_separator();
            console.log('failed to subscribe to topic', err);
        }
    );


};

connection.onclose = function(reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}


console.log("Running AutobahnJS " + autobahn.version);
try{
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
};

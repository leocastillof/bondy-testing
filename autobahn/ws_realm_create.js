#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Get command line arguments
var args = process.argv.slice(2);

// Check if enough arguments were provided
if (args.length < 7) {
    console.error('Usage: node ws_realm_create.js <uri> <description> <is_prototype> <is_sso_realm> <allow_connections> <authmethods>');
    process.exit(1);
}

// Extract arguments
var [uri, description, is_prototype, is_sso_realm, allow_connections, ...authmethods] = args;

// Anonymous connection
var connection = new autobahn.Connection({
   url: "ws://localhost:18080/ws",
   realm: "com.leapsight.bondy",
   authmethods : ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

connection.onopen = function(session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // CREATE realm
    var newRealmConfig = {
        uri: uri,
        description: description,
        is_prototype: Boolean(is_prototype),
        is_sso_realm: Boolean(is_sso_realm),
        allow_connections: Boolean(allow_connections),
        authmethods: authmethods
        // Add other properties as needed
    };

    session.call('bondy.realm.create', [newRealmConfig]).then(
        function (res) {
            log_separator();
            console.log('Success! Result: ', res);
        },
        function (err) {
            log_separator();
            console.log('Error creating the realm...', err);
        }
    );
};

connection.onclose = function(reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

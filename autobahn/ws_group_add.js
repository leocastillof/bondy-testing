#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');
const readline = require('readline');

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function add_group(session, realmUri, groupData) {
    session.call(
        'bondy.group.add',
        [realmUri, groupData]
    ).then(
        function (res) {
            log_separator();
            console.log('Group Added: ', res);
        },
        function (err) {
            log_separator();
            console.log('Failed to add group', err.error, err.kwargs);
        }
    );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // Prompt for realmUri and groupData
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question('Enter the URI of the realm: ', (realmUri) => {
        rl.question('Enter the name of the group: ', (groupName) => {
            // For simplicity, assuming an empty groups array and metamap
            var groupData = {
                name: groupName,
                groups: [],
                metamap: {}
            };

            // Call the function to add the group
            add_group(session, realmUri, groupData);

            // Close the readline interface
            rl.close();
        });
    });
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

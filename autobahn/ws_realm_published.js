#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');
const WebSocket = require('ws');
const readline = require('readline');

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function onPublish(args, kwargs, details) {
    log_separator();
    console.log('Received a publish event on topic:', details.topic);
    console.log('Arguments:', args);
    console.log('Keyword Arguments:', kwargs);
}

function promptForRealmData() {
    return new Promise((resolve, reject) => {
        rl.question('Enter realm URI: ', (realmUri) => {
            rl.question('Enter realm description: ', (description) => {
                const newRealmConfig = {
                    description: description,
                    is_prototype: false
                };
                resolve({ realmUri, newRealmConfig });
            });
        });
    });
}

connection.onopen = async function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // Subscribe to the topic to capture publish events
    session.subscribe('bondy.realm.updated', onPublish);

    // Prompt user for realm data
    const { realmUri, newRealmConfig } = await promptForRealmData();

    log_separator();
    console.log('Publishing to bondy.realm.updated');
    session.publish(
        'bondy.realm.updated',
        [realmUri, newRealmConfig]
    ).then(
        function (res) {
            log_separator();
            console.log('Result: ', res);
        },
        function (err) {
            log_separator();
            console.log('Failed to publish to procedure', err);
        }
    );

    // Close readline interface
    rl.close();
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
}

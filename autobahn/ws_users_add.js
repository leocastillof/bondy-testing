#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');

// Anonymous connection
var connection = new autobahn.Connection({
    url: "ws://localhost:18080/ws",
    realm: "com.leapsight.bondy",
    authmethods: ["anonymous"]
});

function log_separator() {
    console.log("--------------------------------------------------------");
}

function add_user(session, realmUri, userData) {
    session.call('bondy.user.add', [realmUri, userData]).then(
        function (user) {
            log_separator();
            console.log('User added successfully. Result: ', user);
        },
        function (err) {
            log_separator();
            console.log('Error adding user', err);
        }
    );
}

connection.onopen = function (session, details) {
    console.log("Connected: ", JSON.stringify(details));

    // Details
    var userData = {
        username: 'leocastillo',
        groups: ['teamcastillo'],
        // sso_realm_uri: 'com.leocastillo',
        enabled: true,
        metamap: { custom_metadata: 'other_value' },
        password: 'leo123',
        authorized_keys: ['key1', 'key2']
    };

    // Realm for add user
    var realmUri = 'com.leocastillo';
    add_user(session, realmUri, userData);
};

connection.onclose = function (reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}

console.log("Running AutobahnJS " + autobahn.version);
try {
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
};

#!/usr/bin/env node

"use strict";

var AUTOBAHN_DEBUG = true;
var autobahn = require('autobahn');
const WebSocket = require('ws');

// Anonymous connection
var connection = new autobahn.Connection({
   url: "ws://localhost:18080/ws",
   realm: "com.leapsight.bondy",
   authmethods : ["anonymous"]
});


function log_separator() {
    console.log("--------------------------------------------------------");
}

connection.onopen = function(session, details) {
    console.log("Connected: ", JSON.stringify(details));

     // CALL list to Realms
    session.call(
        'bondy.realm.list',
        []
    ).then(
        function (res) {
            log_separator();
            console.log('Result: ', res);
        },
        function (err) {
            log_separator();
            console.log('failed to call to procedure', err);
        }
    );


};

connection.onclose = function(reason, details) {
    log_separator();
    console.log(
        "Connection closed: " + reason +
        " details: " + JSON.stringify(details)
    );
}


console.log("Running AutobahnJS " + autobahn.version);
try{
    connection.open();
} catch (e) {
    console.log("Connection Error " + e);
};
